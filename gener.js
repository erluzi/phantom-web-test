var fs = require('fs'),
    system = require('system'),
    module_name,//文件名: app_gulp
    pro_name;

if(system.args.length<2){
    console.log('please set project directory')
    phantom.exit('please set project directory')
}else{
    module_name = system.args[1]
    pro_name = system.args[2]
    if(fs.exists(pro_name)){
        console.log('project directory is exists')
        phantom.exit('project directory is exists')
    }
}

console.log('use module: ' + module_name)
console.log('project directory: ' + pro_name)
// console.log(fs.workingDirectory)
// fs.changeWorkingDirectory('/Users/cq/Documents/dev')

fs.copyTree(fs.workingDirectory+'/'+module_name+'/', pro_name)

phantom.exit()
// var cwd = fs.absolute('.')
// console.log(cwd)
//
// var parent = fs.absolute("../");
// console.log(parent);