var page = require('webpage').create(),
    system = require('system'), 
    t, address;

phantom.onError = function(msg, trace) {
    var msgStack = ['PHANTOM ERROR: ' + msg];
    if (trace && trace.length) {
        msgStack.push('TRACE:');
        trace.forEach(function(t) {
            msgStack.push(' -> ' + (t.file || t.sourceURL) + ': ' + t.line + (t.function ? ' (in function ' + t.function +')' : ''));
        });
    }
    console.error(msgStack.join('\n'));
    phantom.exit(1);
}

page.onConsoleMessage = function (msg) {
    console.log('log: ' + msg)
}

page.onError = function (msg, trace) {
    console.log('error: ' + msg)
}

if(system.args.length==1){
    page.open('http://h5.tonglvhuanqiu.com/shop/', function (status) {
        if(status==='success'){
            // page.render('tonglv.png')
            page.evaluate(function () {
                console.log(document.title)
            })
        }
        phantom.exit()
    })
}else{
    address = system.args[1];
    t = Date.now();
    page.open(address, function (status) {
        if(status==='success'){
            console.log('loading ' + address)
            console.log('loading time '+ (Date.now() - t))
        }else{
            console.log('fail to load ' + address)
        }
        phantom.exit()
    })
}


