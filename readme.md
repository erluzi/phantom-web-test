- brew install phantomjs
- execute script in package.json
```
- phantom_module (test phantom module, 入口文件: phantom_module/index.js)
- webjs (test js, 入口文件: webjs/index.js)

- app_gulp 模版项目
```

- 拷贝
```
chmod +x ./copy.sh
./copy.sh
```

- 创建模板项目
```
npm run gener
```

- 简单js测试（不支持es6）
```
npm t
```

- node 10 abort
```
rm -rf node_modules
rm -rf package-lock.json
npm cache clean --force
npm i
```
