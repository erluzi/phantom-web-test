function formatNum(s) {
    var str = parseFloat(s).toFixed(2).toString();
    var newStr = "";
    var count = 0;

    for (var i = str.indexOf(".") - 1; i >= 0; i--) {
        if (count % 3 == 0 && count != 0) {
            newStr = str.charAt(i) + "," + newStr;
        } else {
            newStr = str.charAt(i) + newStr; //逐个字符相接起来
        }
        count++;
    }
    return newStr + (str + "00").substr((str + "00").indexOf("."), 3);
}

var r1 = formatNum('132134.2');  //输出132,134.20
var r2 = formatNum('132134');  //输出132,134.00
var r3 = formatNum('132134.236');  //输出132,134.24

console.log(r1)
console.log(r2)
console.log(r3)